#include "slaballoc.h"
#include "objetos.h"


int main(){

	printf("Función: Crear cache\n");
	printf("inicializada... \n");
	SlabAlloc *slbaloc=crear_cache("Thanos",sizeof(Malo),crear_Malo,destruir_Malo);	
	stats_cache(slbaloc);

	printf("\n");
	printf("\n");
	printf("Función: Obtener cache\n");
	printf("inicializada... \n");
	Malo *slabMalo=obtener_cache(slbaloc,1);
	slabMalo->salud=1000;
	slabMalo->fuerza=10000;
	stats_cache(slbaloc);
	
	/*printf("\n");
	printf("\n");
	printf("Función: Obtener cache\n");
	printf("inicializada... \n");
	Malo *slabMalo1=obtener_cache(slbaloc,1);
	slabMalo1->salud=1000;
	slabMalo1->fuerza=10000;
	stats_cache(slbaloc);*/

	printf("\n");
	printf("\n");
	printf("Función: Devolver cache\n");
	printf("inicializada... \n");
	devolver_cache(slbaloc,slabMalo);
	/*devolver_cache(slbaloc,slabMalo1);*/
	stats_cache(slbaloc);

	printf("\n");
	printf("\n");
	printf("Función: Destruir cache\n");
	printf("inicializada... \n");
	destruir_cache(slbaloc);
	stats_cache(slbaloc);
	return 0;
}



