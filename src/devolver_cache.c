#include <stdlib.h>
#include<stdio.h>
#include "slaballoc.h"
#include "objetos.h"

//Devuelve un objeto al cache (se marca el slab  como DISPONIBLE).
//Estos sucede si *obj == slab.ptr_data (sabemos cual es el slab 
//que esta asociado al objeto, y por lo tanto podemos marcar el
//slab como disponible. Cuando marque el objeto como disponible,
//no olvide llamar al destructor ATENCIÓN: YA NO SE DESTRUYE EL OBJETO
void devolver_cache(SlabAlloc *alloc, void *obj){	
	Slab *slab = alloc->slab_ptr;
	for(int i=0;i<alloc->tamano_cache;i++){
		/*printf("obj %p \n",obj);
		printf("%p \n",(slab+i)->ptr_data);
		printf("%p \n",(slab+i)->ptr_data_old);*/
		if(obj==(slab+i)->ptr_data || obj==(slab+i)->ptr_data_old){								
			(slab+i)->status=1; //Recordar que el #1 es para marcar que está disponible
			alloc->cantidad_en_uso=alloc->cantidad_en_uso-1;			
		}		
	}
}
